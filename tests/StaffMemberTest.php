<?php

require_once '../src/StaffMember.php';
class StaffMemberTest extends PHPUnit_Framework_TestCase {
  private  $oStaffMember;
  
  function setUp() {
    parent::setUp();
  }
  
  /**
  * @test
  */
  function StaffMemberHasPropterty() {
    $this->assertClassHasAttribute( 'name'           , 'StaffMember' );
    $this->assertClassHasAttribute( 'discountPercent', 'StaffMember' );
  }
  
  /**  
  * @test  
  */  
  function StaffMemberSetProperty() {
    $this->oStaffMember = new StaffMember( 'Sally', 5 );
    $this->assertEquals( 'Sally', $this->oStaffMember->getName() );
    $this->assertEquals( 5      , $this->oStaffMember->getDiscountPercent() );
  }
  
  /**  
  * @test  
  */  
  function StaffMemberApplyStaffDiscount_TwentyPC_SumTotalLessDiscountPC() {
    $this->oStaffMember = new StaffMember( 'Pluto', 20 );
    $oCashRegister      = new CashRegister();
    
    $oCashRegister->scan( 'eggs'    , 1 );
    $oCashRegister->scan( 'milk'    , 1 );
    $oCashRegister->scan( 'magazine', 3 );
    $iExpectedTotal = ( 1 * 0.98 ) + ( 1 * 1.23 ) + ( 3 * 4.99 ) ;
    $this->assertEquals( $iExpectedTotal
                       , $oCashRegister->getTotal()
                       , 'Before discount'
                       );
    $oCashRegister->applyStaffDiscount( $this->oStaffMember );
    $this->assertEquals( $iExpectedTotal - ( ( 20 / 100 ) * $iExpectedTotal )
                       , $oCashRegister->getTotal()
                       , 'After discount'
                       );
  }
}
